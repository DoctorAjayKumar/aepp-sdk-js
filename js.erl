-module(js).
-export([main/2]).
-export([mainfun/2]).
-export([main/3]).


main(0, Fun) ->
    Fun();
main(N, Fun) ->
    spawn(fun() -> (fun() -> main(N - 1, Fun) end)() end).

mainfun(N, Fun) ->
    Self = self(),
    Pid = spawn(fun() -> main(N, Fun, Self) end),
    Pid ! do.
main(0, Fun, Parent) ->
    receive
        do ->
            Fun(),
            Parent ! {self(), done}
    end;
main(N, Fun, Parent) ->
    Self = self(),
    Pid = spawn(
        fun() ->
            (fun() ->
                main(N - 1, Fun, Self)
            end)()
        end
    ),
    receive
        do ->
            Pid ! do
    end,
    receive
        {Pid, done} ->
            Parent ! {self(), done}
    end.
